# README #

This Project created by students of Computer Science Ain Shams University 

Members :
1- Ibrahim Mohammed Ali Amer
2- Ahmed Saeed Ibrahim
3- Aya Ahmed Serry
4- Mostafa Fouad Mahmoud 
5- Hajer Adel Ahmed 
6- Zainab Mohammed Fouad 

###To Run This Project###

You will need :
1- Windows 7 64-bit 
2- Visual Studio 2012 or higher version
3- MATLAB Compiler Runtime (MCR) (Matlab 2013 or higher version)
4- Multi Core processor 
 
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Summary of set up ###
-You will need to install matlab 2013 first to run this project.
You will need Computer Vision and Image Processing Toolboxes .
- You will also need to install Microsoft Visual Studio 2013 or higher version .WPF is a must 

### To build this project ###
- Open cbvr-ainshams-uni folder then double click on WpfApplication1.sln.
- Then simply press CTRL + F5 to run the project. It may take few seconds before the main form load
- When the main form finish loading press on the play image to choose a video there is an included test case (BBC UK News at 6 - 1.3.mp4) 
  Note : to test on another video we will need to use a commercial software to extract audio file from audio and place it in the same folder
  of video file (separated audio file of the available test case is attached)
- After choosing the video the video file will play automatically you can pause it, forward it, stop it, and you can control the volume level up and down 
- To test the main functionality of the project press (Run Main Modules button) and wait for a while until the main modules finish processing (you will be notified when 
  the main modules finish processing) 
- After you been notified, choose the language for the speech recognition engine from the above menu item called Speech Recognition and choose your video spoken language.
  There are 2 different supported languages (British and American)
- Now press Initialize Speech Recognition Button and wait until speech recognition engine finish listening and recognition (you will be notified when speech recognition engine
  finish listening 
- After you been notified, press Results button to view the results 

### Source Control ###
- We are using BitBucket repository with GIT source control from here you can download the project (https://bitbucket.org/cs14team/cbvr-ainshams-uni/downloads)
  but you will need to add the attached dlls CBVR.dll and MWArray.dll manually. Simply after you open the solution, open solution explorer right click on References
  and press add reference and browse for CBVR.dll and MWArray.dll   

### Who do I talk to? ###

* To contact us :
* ibrahimamer.fcis@gmail.commercial
* Mobile : +201019994392
* Other community or team contact