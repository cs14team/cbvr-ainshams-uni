﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechToText
{
    class Crime : Audio_Recognition_Module
    {
        public Crime()
        {
            Database["crime"] = 0;
            Database["criminal"] = 0;
            Database["addiction"] = 0;
            Database["fraud"] = 0;
            Database["murder"] = 0;
            Database["offender"] = 0;
            Database["forgery"] = 0;
            Database["victim"] = 0;
            Database["witness"] = 0;
            Database["drug"] = 0;
            Database["assault"] = 0;
            Database["burglary"] = 0;
            Database["mugging"] = 0;
            Database["murderer"] = 0;
            Database["rape"] = 0;
            Database["offences"] = 0;
            Database["crimes"] = 0;
            Database["criminals"] = 0;
            Database["commissioner"] = 0;
            Database["criminologist"] = 0;
            Database["prison"] = 0;
            Database["jail"] = 0;
        }
    }
}
