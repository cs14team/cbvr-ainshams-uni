﻿using GP1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechToText
{
    public class Audio_Recognition_Module
    {
        public Dictionary<string,int> Database;
        public TimeSpan startTime;
        public TimeSpan endTime;
        public string recognizedKeyword; // contain all possible word found on the speech
        public string highestRepeatationKeyword; // contain the most repeated word
        public int NumOfRepition; // the value of the highest repeatation word
        public float count; //number of all recognized words
        public float ProbCateg;
        public string CategoryType;
        public Classes LocalCategory;

        public Audio_Recognition_Module()
        {
             Database=new Dictionary<string,int>();
             recognizedKeyword = "";
             highestRepeatationKeyword = "";
             NumOfRepition=0;
             count = 0;
             ProbCateg=0;
        }
        public void searchDB1(string text)
        {
            string[] field;
            text = text.Replace('\r', ' ');
            field = text.Split(' ');
            int length = Database.Count;
            string temp;
            for (int j = 0; j < field.Length; j++)
            {
                if (Database.ContainsKey(field[j].ToLower()))
                {
                    temp = field[j].ToLower();
                    if (Database[temp] == 0)
                    {
                        count++;
                        recognizedKeyword += temp + " ";
                        Database[temp]++;
                      
                    }
                    else if (Database[temp] >= 1)
                    {
                        count++;
                        Database[temp]++;
                    }

                    if (Database[temp] > NumOfRepition)
                    {
                        highestRepeatationKeyword = temp;
                        NumOfRepition = Database[temp];
                    }
                }
            }
        }
        public float getTheCategoryProaility()
        {
            int Num_Keyword_DB = Database.Count;
            int Num_Keyword_recognized=recognizedKeyword.Split(' ').Length;
            float CateProb = Num_Keyword_recognized / (float)Num_Keyword_DB;
            return CateProb;
        }
    }
}
