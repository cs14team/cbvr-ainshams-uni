﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Speech.Recognition;
using System.Windows;
using System.Diagnostics;
using System.Windows.Forms;
using System.Speech.AudioFormat;
using System.Threading;
using NAudio.Wave;
using System.ComponentModel;
using GP1;
using WpfApplication1.Speech_Recognition;

namespace SpeechToText
{
    class SpeechRec
    {

        #region Attributes

        SpeechRecognitionEngine speechRecognitionEngine = null;
        public string speechstring = "";
        public string recognizedword = "";
        public Audio_Recognition_Module[] AllCategory;
        public int highestProbindex = 0;
        bool completed = false;
        double s;
        public string categorytype;
        Dictionary<string, int> Database = new Dictionary<string, int>();
        TimeSpan StartTime;
        TimeSpan EndTime;
        #endregion

        #region Methods and Event

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow1"/> class.
        /// </summary>
        public SpeechRec(TimeSpan StartTime, TimeSpan EndTime, string path ,RecognitionLanguage ChoosedLang)
        {
            //InitializeComponent();
            AllCategory = new Audio_Recognition_Module[4];
            AllCategory[0] = new Sport();
            AllCategory[1] = new Politics();
            AllCategory[2] = new Economy();
            AllCategory[3] = new Weather();
            //AllCategory[4] = new Crime();

            try
            {
                WavSplitter WS = new WavSplitter();
                // create the engine
                if (ChoosedLang == RecognitionLanguage.British)
                {
                speechRecognitionEngine = createSpeechEngine("en-GB");
                }
                else
                {
                speechRecognitionEngine = createSpeechEngine("en-US");
                }

                // hook to events
                speechRecognitionEngine.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(engine_SpeechRecognized);
                speechRecognitionEngine.RecognizeCompleted += new EventHandler<RecognizeCompletedEventArgs>(recognizer_RecognizeCompleted);

                // load dictionary
                speechRecognitionEngine.LoadGrammar(new DictationGrammar());

                // use the system's default microphone
                speechRecognitionEngine.SetInputToWaveFile(path);
                this.StartTime = StartTime;
                this.EndTime = EndTime;
                // start listening for continuous speech recognition 
                speechRecognitionEngine.RecognizeAsync(RecognizeMode.Multiple);
                //double s = WS.AudioDuration;
                s = (-StartTime.TotalSeconds + EndTime.TotalSeconds);
                Thread.Sleep((int)((s) * 1000 / 2.5));
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Voice recognition failed");
            }
        }



        /// <summary>
        /// Creates the speech engine.
        /// </summary>
        /// <param name="preferredCulture">The preferred culture.</param>
        /// <returns></returns>
        private SpeechRecognitionEngine createSpeechEngine(string preferredCulture)
        {
            var x = SpeechRecognitionEngine.InstalledRecognizers();
            foreach (RecognizerInfo config in SpeechRecognitionEngine.InstalledRecognizers())
            {
                if (config.Culture.ToString() == preferredCulture)
                {
                    speechRecognitionEngine = new SpeechRecognitionEngine(config);
                    break;
                }
            }

            // if the desired culture is not found, then load default
            if (speechRecognitionEngine == null)
            {
                System.Windows.MessageBox.Show("The desired culture is not installed on this machine, the speech-engine will continue using "
                    + SpeechRecognitionEngine.InstalledRecognizers()[0].Culture.ToString() + " as the default culture.",
                    "Culture " + preferredCulture + " not found!");
                speechRecognitionEngine = new SpeechRecognitionEngine(SpeechRecognitionEngine.InstalledRecognizers()[0]);
            }

            return speechRecognitionEngine;
        }

        //private void worker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    // run all background tasks here
        //    Thread.Sleep((int)((s) * 1000 / 2));
        //}

        //private void worker_RunWorkerCompleted(object sender,RunWorkerCompletedEventArgs e)
        //{
        //    //update ui once worker complete his work
        //}

        public string getCategory()
        {
            //float highestProb = -1;
            int Index = -1;
            float temp = 0;
            for (int i = 0; i < AllCategory.Length; i++)
            {
                temp += AllCategory[i].count;
            }

            for (int i = 0; i < AllCategory.Length; i++)
            {
                AllCategory[i].ProbCateg = AllCategory[i].count / temp;
            }
            Index = getHighestProb();
            if (Index > -1)
            {
                Type t = AllCategory[Index].GetType();
                return t.Name;
            }
            else
            {
                return "";
            }

        }

        int getHighestProb()
        {
            int Index = -1;
            float highestProb = -1;
            for (int i = 0; i < AllCategory.Length; i++)
            {
                if (highestProb < AllCategory[i].ProbCateg)
                {
                    highestProb = AllCategory[i].ProbCateg;
                    Index = i;
                }
            }
            this.highestProbindex = Index;
            return Index;
        }

        void keyWordRecognition()
        {
            for (int i = 0; i < AllCategory.Length; i++)
            {
                AllCategory[i].searchDB1(getSpeechText());
            }
            //if (highestProbindex >= 0)
            //{
            //    AllCategory[highestProbindex].recognizedKeyword = AllCategory[highestProbindex].recognizedKeyword;
            //}
            //else
            //{
            //    AllCategory[highestProbindex].recognizedKeyword = "";
            //}
        }
        public void getinfo()
        {
            this.keyWordRecognition();
            categorytype = getCategory();
            AllCategory[highestProbindex].CategoryType = this.categorytype;
            AllCategory[highestProbindex].startTime = this.StartTime;
            AllCategory[highestProbindex].endTime = this.EndTime;
            if (this.categorytype.ToLower() == "sport"){
                AllCategory[highestProbindex].LocalCategory = Classes.Sport;
            }
            else if (this.categorytype.ToLower() == "politics"){
                AllCategory[highestProbindex].LocalCategory = Classes.Politics;
            }
            else if (this.categorytype.ToLower() == "weather"){
                AllCategory[highestProbindex].LocalCategory = Classes.Weather;
            }
            else
                AllCategory[highestProbindex].LocalCategory = Classes.Economy;
            //this.highestProbindex = getHighestProb();          
            //ListOfCategory.Add(AllCategory[highestProbindex]);
        }

        public bool isCompleted()
        {
            return completed;
        }
        public string getSpeechText()
        {
            return speechstring;
        }

        #endregion

        #region speechEngine events

        /// <summary>
        /// Handles the SpeechRecognized event of the engine control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Speech.Recognition.SpeechRecognizedEventArgs"/> instance containing the event data.</param>
        void engine_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            speechstring += "\r" + e.Result.Text;
        }

        /// <summary>
        /// Handles the Closing event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // unhook events
            speechRecognitionEngine.RecognizeAsyncStop();
            // clean references
            speechRecognitionEngine.Dispose();
        }

        void recognizer_RecognizeCompleted(object sender, RecognizeCompletedEventArgs e)
        {
            completed = true;
            //speechRecognitionEngine.RecognizeAsyncStop();
            //speechstring =e.Result.Text;
        }

        void recognizer_LoadGrammarCompleted(object sender, LoadGrammarCompletedEventArgs e)
        {
            Console.WriteLine("Grammar loaded:  " + e.Grammar.Name);
        }
        #endregion
    }
}