﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechToText
{
    public class Sport : Audio_Recognition_Module
    {
        public Sport()
        {
            Database["captain"] = 0;
            Database["championship"] = 0;
            Database["championships"] = 0;
            Database["competition"] = 0;
            Database["competitor"] = 0;
            Database["contest"] = 0;
            Database["cup"] = 0;
            Database["defeat"] = 0;
            Database["draw"] = 0;
            Database["fitness"] = 0;
            Database["gym"] = 0;
            Database["league"] = 0;
            Database["goal"] = 0;
            Database["score"] = 0;
            Database["medal"] = 0;
            Database["penalty"] = 0;
            Database["performance"] = 0;
            Database["player"] = 0;
            Database["shoot"] = 0;
            Database["stadium"] = 0;
            Database["tactics"] = 0;
            Database["trainers"] = 0;
            Database["victory"] = 0;
            Database["trophy"] = 0;
            Database["tournament"] = 0;
            Database["tackle"] = 0;
            Database["serve"] = 0;
            Database["coach"] = 0;
            Database["game"] = 0;
            Database["serena"] = 0;
            Database["football"] = 0;
            Database["basketball"] = 0;
            Database["handball"] = 0;
            Database["tennis"] = 0;
            Database["wimbledon"] = 0;
            Database["ski"] = 0;
            Database["olympics"]=0;
            Database["olympic"] = 0;
        }
    }
}
