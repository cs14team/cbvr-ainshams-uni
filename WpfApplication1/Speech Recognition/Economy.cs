﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechToText
{
    class Economy : Audio_Recognition_Module
    {
        public Economy()
        {
            Database["economic"] = 0;
            Database["economics"]=0;
            Database["economies"] = 0;
            Database["currency"] = 0;
            Database["financial"] = 0;
            Database["aid"] = 0;
            Database["exchange"] = 0;
            Database["tax"] = 0;
            Database["taxes"] = 0;
            Database["autarky"] = 0;
            Database["auctions"] = 0;
            Database["advertising"] = 0;
            Database["amortization"] = 0;
            Database["arbitrage"] = 0;
            Database["barter"] = 0;
            Database["benefit"] = 0;
            Database["bond"] = 0;
            Database["firm"] = 0;

            Database["Costs"] = 0;
            Database["Demand"] = 0;
            Database["Entrepreneur"] = 0;
            Database["Entrepreneurship"] = 0;
            Database["Goods"] = 0;
            Database["Imports"] = 0;
            Database["income"] = 0;
            Database["investment"] = 0;
            Database["investments"] = 0;
            Database["invest"] = 0;
            Database["market"] = 0;
            Database["money"] = 0;
            Database["producers"] = 0;
            Database["production"] = 0;
            Database["production"] = 0;
            Database["stock"]=0;
            Database["stocks"] = 0;

            Database["profit"] = 0;
            Database["saving"] = 0;
            Database["scarcity"] = 0;
            Database["shortage"] = 0;
            Database["stock"] = 0;
            Database["supply"] = 0;
            Database["surplus"] = 0;
            Database["tariff"] = 0;
            Database["trade"] = 0;
            Database["wages"] = 0;
            Database["exports"] = 0;
            Database["business"] = 0;
            Database["economy"] = 0;
            Database["loan"] = 0;
        }
    }
}
