﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechToText
{
    class Weather : Audio_Recognition_Module
    {
        public Weather()
        {
            Database["weather"] = 0;
            Database["climate"] = 0;
            Database["fog"] = 0;
            Database["snowing"] = 0;
            Database["temperature"] = 0;
            Database["temperatures"] = 0;
            Database["celsius"] = 0;
            Database["fahrenheit"] = 0;
            Database["humidity"] = 0;
            Database["pressure"] = 0;
            Database["spring"] = 0;
            Database["summer"] = 0;
            Database["winter"] = 0;
            Database["autumn"] = 0;
            Database["wind"] = 0;
            Database["rain"] = 0;
            Database["foggy"] = 0;
            Database["rainy"] = 0;
            Database["raining"] = 0;
            Database["cloudy"] = 0;
            Database["snowy"] = 0;
            Database["snow"] = 0;
            Database["cloud"] = 0;
            Database["sunny"] = 0;
            Database["sun"] = 0;
            Database["clouds"] = 0;
            Database["country"] = 0;
            Database["thunder"] = 0;
            Database["lightning"] = 0;
            Database["moon"] = 0;
            Database["windy"] = 0;
            Database["season"] = 0;
            Database["seasons"] = 0;
            Database["freezing"] = 0;

            
         

        }
    }
}
