﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechToText
{
    class WavSplitter
    {
        TimeSpan StartTime;
        TimeSpan EndTime;
        public TimeSpan AudioDuration;
        public string fileSplitPath;
        public List<string> path;
        WaveFileReader reader;
        public WavSplitter()
        {
            this.StartTime = TimeSpan.Zero;
            this.EndTime = TimeSpan.Zero;
            this.AudioDuration = TimeSpan.Zero;
            path = new List<string>();
        }

        public void TrimTheAudioFile(List<TimeSpan> StartTime, string inPath, string outpath)
        {
            using (reader = new WaveFileReader(inPath))
            {
                this.AudioDuration = reader.TotalTime;
                for (int i = 0; i < StartTime.Count; i++)
                {
                    if (i != StartTime.Count -1)
                    {
                        TrimWavFile(inPath, "segment" + i, outpath, StartTime.ElementAt(i), StartTime.ElementAt(i + 1));
                    }
                    else
                    {
                        TrimWavFile(inPath, "segment" + i, outpath, StartTime.ElementAt(i), AudioDuration - TimeSpan.FromMilliseconds(300));
                    }
                }
            }
        }

        public void TrimWavFile(string inPath, string segment, string outPath, TimeSpan cutFromStart, TimeSpan cutFromEnd)
        {

            using (WaveFileWriter writer = new WaveFileWriter(outPath + "\\" + Path.GetFileNameWithoutExtension(inPath) + segment + ToString().PadLeft(5, Convert.ToChar("0")) + Path.GetExtension(inPath), reader.WaveFormat))
            {
                path.Add(writer.Filename);
                fileSplitPath = writer.Filename;
                int bytesPerMillisecond = reader.WaveFormat.AverageBytesPerSecond / 1000;

                int startPos = (int)cutFromStart.TotalMilliseconds * bytesPerMillisecond;
                startPos = startPos - startPos % reader.WaveFormat.BlockAlign;

                int endBytes = (int)cutFromEnd.TotalMilliseconds * bytesPerMillisecond;
                endBytes = endBytes - endBytes % reader.WaveFormat.BlockAlign;
                int endPos = (int)reader.Length - endBytes;

                this.TrimWavFile1(reader, writer, startPos, endBytes);
            }

        }
        private void TrimWavFile1(WaveFileReader reader, WaveFileWriter writer, int startPos, int endPos)
        {
            reader.Position = startPos;
            byte[] buffer = new byte[1024];
            while (reader.Position < endPos)
            {
                int bytesRequired = (int)(endPos - reader.Position);
                if (bytesRequired > 0)
                {
                    int bytesToRead = Math.Min(bytesRequired, buffer.Length);
                    int bytesRead = reader.Read(buffer, 0, bytesToRead);
                    if (bytesRead > 0)
                    {
                        writer.WriteData(buffer, 0, bytesRead);
                    }
                }
            }
        }
    }
}
