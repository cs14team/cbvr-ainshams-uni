﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechToText
{
    class Politics : Audio_Recognition_Module
    {
        public Politics()
        {
            Database["armed"] = 0;
            Database["elections"] = 0;
            Database["politicians"] = 0;
            Database["protest"] = 0;
            Database["protests"] = 0;
            Database["president"] = 0;
            Database["minister"] = 0;
            Database["defense"] = 0;
            Database["government"] = 0;
            Database["party"] = 0;
            Database["civil"] = 0;
            Database["libel"] = 0;
            Database["confederation"] = 0;
            Database["politics"] = 0;
            Database["army"] = 0;
            Database["authority"] = 0;
            Database["democracy"] = 0;
            Database["elected"] = 0;
            Database["suffrage"] = 0;
            Database["legitimacy"] = 0;
            Database["ideology"] = 0;
            Database["partnership"] = 0;
            Database["revolution"] = 0;
            Database["war"] = 0;
            Database["country"] = 0;
            Database["nuclear"] = 0;
            Database["military"] = 0;
            //Database["police"] = 0;
            Database["ministry"] = 0;
            Database["absolutism"] = 0;
            Database["anarchism"] = 0;
            Database["appropriation"] = 0;
            Database["authorization"] = 0;
            Database["autocracy"] = 0;
            Database["bill"] = 0;
            Database["bureaucracy"] = 0;
            Database["casework"] = 0;
            Database["cloture"] = 0;
            Database["Politically"] = 0;
            Database["communism"] = 0;
            Database["constitution"] = 0;
            Database["forces"] = 0;
            Database["resign"] = 0;

        }
    }
}
