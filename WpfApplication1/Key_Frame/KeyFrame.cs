﻿namespace WpfApplication1.Key_Frame
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media.Imaging;

    public class KeyFrame
    {
        public volatile BitmapImage keyframe;
        public volatile List<string> personsNames;
        public volatile List<string> categories;

        public KeyFrame(BitmapImage keyframe, List<string> personsNames, List<string> categories)
        {
            this.keyframe = keyframe;
            this.keyframe.Freeze();
            this.personsNames = personsNames;
            this.categories = categories;
        }
    }
}
