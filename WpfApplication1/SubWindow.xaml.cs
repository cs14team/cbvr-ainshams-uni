﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for SubWindow.xaml
    /// </summary>
    public partial class SubWindow : Window
    {

        bool isDragging = false;
        bool isVolEnable = true;
        DispatcherTimer timer;
        private TimeSpan TotalTime;
        TimeSpan videopos;
        string source;
        public SubWindow(string source)
        {
            InitializeComponent();
            this.source = source;

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(1000);
            timer.Tick += new EventHandler(timer_Tick);
            this.SlideBar.Background = new SolidColorBrush(Colors.Black);

            playVideo();
        }

        public void playVideo()
        {
                  
            Media.Source = new Uri(source);
            this.PlayBGImage.Visibility = Visibility.Hidden;
            Media.Play();
        }

        void Media_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            // Handle failed media event

        }

        void Media_MediaOpened(object sender, RoutedEventArgs e)
        {

            //this.SlideBar.Value+=Media.
            TotalTime = Media.NaturalDuration.TimeSpan;
            // Create a timer that will update the counters and the time slider
            if (Media.NaturalDuration.HasTimeSpan)
            {
                TimeSpan ts = Media.NaturalDuration.TimeSpan;
                SlideBar.Maximum = ts.TotalSeconds;
                SlideBar.SmallChange = 1;
                SlideBar.LargeChange = Math.Min(10, ts.Seconds / 10);
            }
            timer.Start();
        }

        void Media_MediaEnded(object sender, RoutedEventArgs e)
        {
            // Handle media ended event
            Media.Stop();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (!isDragging)
            {
                SlideBar.Value = Media.Position.TotalSeconds;
                this.Timelbl.Content = Media.Position.Hours.ToString() + ":" + Media.Position.Minutes.ToString() + ":" + Media.Position.Seconds.ToString();
                videopos=Media.Position;
            }
        }
       

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Slider_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.Media.Volume = this.VolumeSlider.Value;
        }
        private void Image_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            string srcStr = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(
                  System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)));
            if (isVolEnable)
            {
                this.Media.Volume = 0;
                this.VolumeSlider.Value = 0;
                isVolEnable = false;
                this.VolImg.Source = new BitmapImage(new Uri(srcStr + "\\Image\\speaker_mute.png"));

            }
            else
            {
                this.Media.Volume = 0.5;
                this.VolumeSlider.Value = 0.5;
                isVolEnable = true;
                this.VolImg.Source = new BitmapImage(new Uri(srcStr + "\\Image\\Volume.png"));
            }
        }

        private void Image_MouseDown_Play3(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.PlayBGImage.Visibility = Visibility.Hidden;
                Media.Play();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Image_MouseDown_Pause3(object sender, MouseButtonEventArgs e)
        {
            if (Media.CanPause)
            {
                Media.Pause();
            }
        }

        private void Image_MouseDown_Stop3(object sender, MouseButtonEventArgs e)
        {

            Media.Stop();
            PlayBGImage.Visibility = Visibility.Visible;
        }

        private void SlideBar_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            isDragging = true;
        }

        private void SlideBar_DragStarted_1(object sender, DragStartedEventArgs e)
        {
            isDragging = false;
            Media.Position = TimeSpan.FromSeconds(SlideBar.Value);
        }

        private void SlideBar_DragCompleted_1(object sender, DragCompletedEventArgs e)
        {

        }
    }
}
