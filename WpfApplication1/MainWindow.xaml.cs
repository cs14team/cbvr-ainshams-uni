﻿namespace WpfApplication1
{
    using GP1;
    using GP1.Modules;
    using Microsoft.Win32;
    using SpeechToText;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using WpfApplication1.Speech_Recognition;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Attribute
        bool isDragging = false;
        bool isVolEnable = true;
        string outpathAudio;
        string Inpath;
        string InpathAudio;
        private string srcStr;
        string removeString;
        string lif;
        string keyframefolderpath;
        string trdbpath;
        string labelspath;
        string rrankspath;
        string alignmatpath;
        private TimeSpan TotalTime;
        Thread MainModuleThread;
        RecognitionLanguage ChoosedLang;
        CBVR.MainModules mainModuleObj;
         MainModules MainModule;
         Lazy<MainModules> LazyMainModule;
        SpeechRec[] SR;
        Uri sourceUri;
        DispatcherTimer timer;
        FormCategory fCategory;
        List<Audio_Recognition_Module> AudioCategoryList;
        WavSplitter WS;
        List<TimeSpan> ShotsInterval;
        List<TextBox> TextBoxList;
        List<StackPanel> StackPanelList;
        
        # endregion

        #region constructor
        public MainWindow()
        {
            InitializeComponent();
            srcStr = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Path.GetDirectoryName
                    (System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)));

            removeString = "file:\\";
            int i = this.srcStr.IndexOf(removeString);
            this.srcStr = (i < 0) ? this.srcStr : this.srcStr.Remove(i, removeString.Length);

            lif = this.srcStr + @"\Matrices\LLF .MatFile\";

            trdbpath = this.srcStr + @"\Matrices\FaceRec\database.mat";
            labelspath = this.srcStr + @"\Matrices\FaceRec\labels.mat";
            rrankspath = this.srcStr + @"\Matrices\FaceRec\lfw.mat";
            alignmatpath = this.srcStr + @"\Matrices\Alignment\face_p99.mat";

            mainModuleObj = new CBVR.MainModules();
           
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(1000);
            timer.Tick += new EventHandler(timer_Tick);
            this.SlideBar.Background = new SolidColorBrush(Colors.Black);
            
        }
        # endregion

        void Media_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            // Handle failed media event
        }

        void Media_MediaOpened(object sender, RoutedEventArgs e)
        {

            //this.SlideBar.Value+=Media.
            TotalTime = Media.NaturalDuration.TimeSpan;
            // Create a timer that will update the counters and the time slider
            if (Media.NaturalDuration.HasTimeSpan)
            {
                TimeSpan ts = Media.NaturalDuration.TimeSpan;
                SlideBar.Maximum = ts.TotalSeconds;
                SlideBar.SmallChange = 1;
                SlideBar.LargeChange = Math.Min(10, ts.Seconds / 10);
            }
            timer.Start();

        }

        void Media_MediaEnded(object sender, RoutedEventArgs e)
        {
            // Handle media ended event
            Media.Stop();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            OpenMedia();
        }

        private void SlideBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
        void timer_Tick(object sender, EventArgs e)
        {
            if (!isDragging)
            {
                SlideBar.Value = Media.Position.TotalSeconds;
                this.Timelbl.Content = Media.Position.Hours.ToString() + ":" + Media.Position.Minutes.ToString() + ":" + Media.Position.Seconds.ToString();
            }
        }
        private void seekBar_DragStarted(object sender, DragStartedEventArgs e)
        {
            isDragging = true;
        }
        private void seekBar_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            isDragging = false;
            Media.Position = TimeSpan.FromSeconds(SlideBar.Value);
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Results_BtnClick(object sender, RoutedEventArgs e)
        {
            InitilizeCategoryForm();
            for (int i = 0; i < SR.Length; i++)
            {
                SR[i].getinfo();
                AudioCategoryList.Add(SR[i].AllCategory[SR[i].highestProbindex]);
            }
          
            MainModule.startMakingDecision(AudioCategoryList);
            CreateCateLbl( (float)(this.SlideBar.Width / WS.AudioDuration.TotalSeconds));

            for (int i = 0; i < ShotsInterval.Count; i++)
            {
                ((TabItem)fCategory.CategTabCont.Items[i]).Visibility = Visibility.Visible;
                ((TabItem)fCategory.CategTabCont.Items[i]).Header = MainModule.video.Cotegories.ElementAt(i).GlobalCategory;
                this.TextBoxList.ElementAt(i).Text = MainModule.video.Cotegories.ElementAt(i).Audio_Recognition_Module_.recognizedKeyword;
            }

            
            Image img;
            for (int i = 0; i < MainModule.faceModuleOutput.Count; i++)
            {
                for (int j = 0; j < MainModule.faceModuleOutput[i].keyframes.Count; j++)
                {
                    lock (this.LazyMainModule.Value)
                    {
                        img = new Image();
                        img.Source = this.LazyMainModule.Value.faceModuleOutput[i].keyframes[j].keyframe.Clone();
                        img.MouseDown += new MouseButtonEventHandler(KeyFrame_MouseDown);
                        this.StackPanelList[i].Children.Add(img); 
                    }
                }
            } 
            
            fCategory.Show();
        }

        void InitilizeCategoryForm()
        {
            TextBoxList = new List<TextBox>();
            StackPanelList = new List<StackPanel>();
            fCategory = new FormCategory();
            foreach (var tab in fCategory.CategTabCont.Items)
            {
                (tab as TabItem).Visibility = Visibility.Hidden;
            }
             TextBoxList.Add(fCategory.TextCateg1);
            TextBoxList.Add(fCategory.TextCateg2);
            TextBoxList.Add(fCategory.TextCateg3);
            TextBoxList.Add(fCategory.TextCateg4);

            StackPanelList.Add(fCategory.StackPanelCate1);
            StackPanelList.Add(fCategory.StackPanelCate2);
            StackPanelList.Add(fCategory.StackPanelCate3);
            StackPanelList.Add(fCategory.StackPanelCate4);
        }

        public bool OpenMedia()
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            // Show the open file dialog
            openDialog.Title = "Open source file";
            openDialog.Filter = "Media files|*.AVI;*.MP4;*.WAV;*.3GP;*.MOV;*.3G2;*.WMV;*.ASF;*.MP3;*.WMA;*.M4A;*.AAC;|All files (*.*)|*.*;";
            if (openDialog.ShowDialog() == false)
            {
                return false;
            }
            string fileExtension = new System.IO.FileInfo(openDialog.FileName).Extension.ToLowerInvariant();
            this.Media.ScrubbingEnabled = fileExtension == ".avi" || fileExtension == ".mp4" || fileExtension == ".3gp" ||
                                          fileExtension == ".mov" || fileExtension == ".mpg" || fileExtension == ".mpeg" ||
                                          fileExtension == ".m2ts" || fileExtension == ".ts" || fileExtension == ".wmv" ||
                                          fileExtension == ".asf";
            // Open the media
            sourceUri = new Uri(openDialog.FileName);
            Media.Source = sourceUri;
            this.PlayBGImage.Visibility = Visibility.Hidden;
            this.StackPanelLbl.Children.Clear();
            this.RMM_Btn.IsEnabled = true;
            this.InitSpeechRec_Btn.IsEnabled = false;
            this.Results_Btn.IsEnabled = false;
            this.Media.Play();
            this.Media.Volume = 0.5;
            this.Inpath = openDialog.FileName;
            this.InpathAudio = System.IO.Path.ChangeExtension(Inpath, ".wav");
            this.outpathAudio = System.IO.Path.GetDirectoryName(Inpath);
            return true;
        }

        private void Slider_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.Media.Volume = this.VolumeSlider.Value;
        }

        private void Media_DragLeave_1(object sender, DragEventArgs e)
        {
            string x = (string)e.Source;
        }

        private void Image_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            if (isVolEnable)
            {
                this.Media.Volume = 0;
                this.VolumeSlider.Value = 0;
                isVolEnable = false;
                this.VolImg.Source = new BitmapImage(new Uri(srcStr + "\\Image\\speaker_mute.png"));
            }
            else
            {
                this.Media.Volume = 0.5;
                this.VolumeSlider.Value = 0.5;
                isVolEnable = true;
                this.VolImg.Source = new BitmapImage(new Uri(srcStr + "\\Image\\volume.png"));
            }
        }

        private void OpenVideo_ImageClick(object sender, MouseButtonEventArgs e)
        {
            if (!OpenMedia())
            {
                return;
            }
            string currentDir = Path.GetDirectoryName(this.Inpath);
            string folderName = currentDir + @"\" + Path.GetFileNameWithoutExtension(this.Inpath);

            if (!Directory.Exists(folderName))
                Directory.CreateDirectory(folderName);
            else
            {
                System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(folderName);
                foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                {
                    file.Delete();
                }
            }
            
            this.keyframefolderpath = folderName;
            this.InitSpeechRec_Btn.IsEnabled = false;
            this.Results_Btn.IsEnabled = false;
            this.RMM_Btn.IsEnabled = true;
            PlayBGImage.Visibility = Visibility.Hidden;
            this.Media.Play();
        }

        private void Image_MouseDown_Play3(object sender, MouseButtonEventArgs e)
        {
            try
            {
                PlayBGImage.Visibility = Visibility.Hidden;
                Media.Play();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Image_MouseDown_Pause3(object sender, MouseButtonEventArgs e)
        {
            if (Media.CanPause)
            {
                Media.Pause();
            }
        }

        private void Image_MouseDown_Stop3(object sender, MouseButtonEventArgs e)
        {
            Media.Stop();
            PlayBGImage.Visibility = Visibility.Visible;
        }

        void InitilizeSpeechRecognition(double[] ShotPosSec)
        {
            AudioCategoryList = new List<Audio_Recognition_Module>();
            ShotsInterval = new List<TimeSpan>();
            WS = new WavSplitter();
            for (int i = 0; i < ShotPosSec.Length; i++)
            {
                ShotsInterval.Add(new TimeSpan(0, 0, (int)ShotPosSec[i]));
            }

            ShotsInterval.Insert(0, TimeSpan.Zero);
            WS.TrimTheAudioFile(ShotsInterval, InpathAudio, outpathAudio);
            SR = new SpeechRec[ShotPosSec.Length + 1];

            for (int i = 0; i < SR.Length; i++)
            {
                if (i != SR.Length - 1)
                    SR[i] = new SpeechRec(ShotsInterval.ElementAt(i), ShotsInterval.ElementAt(i + 1), WS.path.ElementAt(i), ChoosedLang);
                else
                    SR[i] = new SpeechRec(ShotsInterval.ElementAt(i), WS.AudioDuration, WS.path.ElementAt(i), ChoosedLang);
            }
        }

        void CreateCateLbl(float Rate)
        {
            // Creat the stack :
            this.StackPanelLbl.Orientation = Orientation.Horizontal;
            double[] widths = new double[ShotsInterval.Count];

            //Get Width :  
            for (int i = 0; i < ShotsInterval.Count; i++)
            {
                if (i != ShotsInterval.Count - 1)
                    widths[i] = (ShotsInterval.ElementAt(i + 1).TotalSeconds - ShotsInterval.ElementAt(i).TotalSeconds);
                else
                    widths[i] = (WS.AudioDuration.TotalSeconds - ShotsInterval.ElementAt(i).TotalSeconds);
                widths[i] = widths[i] * Rate;
            }
            List<SolidColorBrush> Col = new List<SolidColorBrush>();
            Col.Add(new SolidColorBrush(Colors.SteelBlue));
            Col.Add(new SolidColorBrush(Colors.Blue));
            Col.Add(new SolidColorBrush(Colors.Tan));
            Col.Add(new SolidColorBrush(Colors.Tomato));
            // Draw label : 
            Label[] Lbl = new Label[ShotsInterval.Count];
            for (int i = 0; i < ShotsInterval.Count; i++)
            {
                Lbl[i] = new Label();
                Lbl[i].Content = MainModule.video.Cotegories.ElementAt(i).GlobalCategory;
                Lbl[i].Width = widths[i];
                Lbl[i].Height = 25;
                Lbl[i].Foreground = new SolidColorBrush(Colors.White);
                Classes c = MainModule.video.Cotegories.ElementAt(i).GlobalCategory;
                if (c == Classes.Sport)
                {
                    Lbl[i].Background = Col.ElementAt(0);
                }
                else if (c == Classes.Politics)
                {
                    Lbl[i].Background = Col.ElementAt(1);
                }
                else if (c == Classes.Economy)
                {
                    Lbl[i].Background = Col.ElementAt(2);
                }
                else
                {
                    Lbl[i].Background = Col.ElementAt(3);
                }
                StackPanelLbl.Children.Add(Lbl[i]);
            }

        }

        public List<BitmapImage> ReadImagesFromDir(string path)
        {
            List<BitmapImage> images = new List<BitmapImage>();
            string[] files = Directory.GetFiles(path);
            if (files.Length == 0)
                return null;
            int count = 0;
            foreach (var file in files)
            {
                images.Add(new BitmapImage(new Uri(file)));
                Image img = new Image();
                img.Source = images.ElementAt(count);
                img.MouseDown += new MouseButtonEventHandler(KeyFrame_MouseDown);
                fCategory.StackPanelCate1.Children.Add(img);
                count++;
            }
            return images;
        }

        private void KeyFrame_MouseDown(object sender, RoutedEventArgs e)
        {
            Image img = (Image)(e.Source);
            int imgIndex = 0;
            for (int k = 0; k < StackPanelList.Count; k++)
            {
                imgIndex = StackPanelList.ElementAt(k).Children.IndexOf(img);
                if (imgIndex != -1)
                {
                    break;
                }
            }
            VisualTreeHelper.GetParent(img);
            var parent = VisualTreeHelper.GetParent(img) as StackPanel;
            int i = StackPanelList.IndexOf(parent);
            List<string> names = MainModule.faceModuleOutput[i].keyframes[imgIndex].personsNames;
            FaceRecForm faceForm = new FaceRecForm();
            for (int k = 0; k < names.Count; k++)
            {
                faceForm.Tblock.Text += (k + 1) + "- " + names[k] + ".\n"; ;
            }
            faceForm.Show();
        }

        private void MenuItem_En_Event(object sender, RoutedEventArgs e)
        {

            MenuItem_En.IsChecked = true;
            MenuItem_GB.IsChecked = false;
            ChoosedLang = RecognitionLanguage.English;
        }

        private void MenuItem_GB_Event(object sender, RoutedEventArgs e)
        {

            MenuItem_GB.IsChecked = true;
            MenuItem_En.IsChecked = false;
            ChoosedLang = RecognitionLanguage.British;
        }

        public void RunMainModules(Object obj)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                this.RMM_Btn.IsEnabled = false;
            }));
            MainModules mm = LazyMainModule.Value;
            mm.RunMainModules();
            this.Dispatcher.Invoke((Action)(() =>
            {
                this.InitSpeechRec_Btn.IsEnabled = true;
                this.RMM_Btn.IsEnabled = false;
                MessageBox.Show("Main Modules has finished processing successfully", "Notification", MessageBoxButton.OK, MessageBoxImage.Information);

            }));
        }

        public void RunInitSpeechRecognition()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                this.InitSpeechRec_Btn.IsEnabled = false;
            }));
            this.InitilizeSpeechRecognition(MainModule.CutPosSec);
            this.Dispatcher.Invoke((Action)(() =>
            {
                this.Results_Btn.IsEnabled = true;
                this.InitSpeechRec_Btn.IsEnabled = false;
                MessageBox.Show("Speech Recognition Engine has finished processing successfully", "Notification", MessageBoxButton.OK, MessageBoxImage.Information);
            }));
        }

        private void RunMainModules_BtnClick(object sender, RoutedEventArgs e)
        {
            this.LazyMainModule = new Lazy<MainModules>(this.InitLazyMainModule);
            MainModuleThread = new Thread(this.RunMainModules);
            MainModuleThread.Start(MainModule);
            
        }

        public MainModules InitLazyMainModule()
        {
            return new MainModules(mainModuleObj, Inpath, lif, keyframefolderpath, trdbpath, labelspath, rrankspath, alignmatpath, 11, 10);
        }

        private void InitSpeechRec_Btn_Click(object sender, RoutedEventArgs e)
        {
            if (!MenuItem_En.IsChecked == true && !MenuItem_GB.IsChecked == true)
            {
                MessageBox.Show("Please choose the recognition langauge. \n go to Recognition Language then go to Choose Langage");
                return;
            }
            MainModuleThread.Abort();
            Thread SRThread = new Thread(this.RunInitSpeechRecognition);
            this.MainModule = this.LazyMainModule.Value;
            SRThread.Start();
        }
    }
}
