﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
namespace GP1
{
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using System.Windows.Media;
    using System.Windows.Controls;

    public class LowLevelModule
    {
        //public List<BitmapImage> KeyFrames;
        public Classes LocalCategory;
        public double Probability;
        public LowLevelModule(double Probability  , string class_)
        {
            //KeyFrames = new List<BitmapImage>();
            this.Probability = Probability;

            if (class_ == "Sport")
                LocalCategory = Classes.Sport;
            else if (class_ == "Politics")
                LocalCategory = Classes.Politics;
            else if (class_ == "Weather")
                LocalCategory = Classes.Weather;
            else if (class_ == "Economy")
                LocalCategory = Classes.Economy;            
        }
    }    
}
