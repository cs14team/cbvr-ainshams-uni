﻿namespace GP1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media.Imaging;
    using System.Windows.Media;
    using System.Windows.Controls;
    using SpeechToText;

    public class Category
    {
        public LowLevelModule Low_Level_Module_;
        public FaceRecognitionModule Face_Recognition_Module_;
        public Audio_Recognition_Module Audio_Recognition_Module_;
        
        float From, To;
        public Classes GlobalCategory;
        public float[] Seconds;

        public Category(LowLevelModule Low_Level_Module_, FaceRecognitionModule Face_Recognition_Module_, Audio_Recognition_Module Audio_Recognition_Module_)
        {
            this.Low_Level_Module_ = Low_Level_Module_;
            this.Face_Recognition_Module_ = Face_Recognition_Module_;
            this.Audio_Recognition_Module_ = Audio_Recognition_Module_;
            MakeDecision();
        }

        void MakeDecision()
        {
            
            if ((Audio_Recognition_Module_.LocalCategory == Low_Level_Module_.LocalCategory))
            {
                GlobalCategory = Audio_Recognition_Module_.LocalCategory;
            }    
            else
            {
                if((Audio_Recognition_Module_.ProbCateg+0.1)>=Low_Level_Module_.Probability)
                    GlobalCategory = Audio_Recognition_Module_.LocalCategory;
                else
                    GlobalCategory = Low_Level_Module_.LocalCategory;
                
            }
        }
    }
    
}
