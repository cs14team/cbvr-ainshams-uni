﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Controls;

namespace GP1.Adapter
{
    class Adapter
    {
        string MainPath;
        string VideoPath;

        private string LLF_DatabasePath;
        private string FR_DatabasePath;
        public string LLF_KeyFramesPath;

        public int TotalNumberOfShots;
        public int [] CutPositions;
        public float[] CategoryBoundaries;
        Image[] keyFrames;

        public Adapter(string MainPath , string VideoPath)
        {
            this.MainPath = MainPath;
            GeneratePaths();
            LinkWithMatlab();
            ReadKeyFrames();
        }
        
        void GeneratePaths()
        {
            // @Ibrahim Check this 
            LLF_DatabasePath  = MainPath + "\\FaceRecognItion\\DataBase";
            FR_DatabasePath   = MainPath + "\\LowLevelFeature\\DataBase";
            LLF_KeyFramesPath = MainPath + "\\LowLevelFeature\\KeyFrames";
        }
        
        public void LinkWithMatlab()
        {
        }
        
        public void ReadKeyFrames()
        {
            keyFrames = new Image[TotalNumberOfShots];
            for (int i = 0; i < TotalNumberOfShots; i++)
            {
                Image im = new Image();
                ImageSource imageSource = new BitmapImage(new Uri(LLF_KeyFramesPath+(CutPositions[i].ToString())));
                im.Source = imageSource;
                keyFrames[i] = im;
            }
        }       
    }
}