﻿namespace GP1.Modules
{
    using MathWorks.MATLAB.NET.Arrays;
    using SpeechToText;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Windows.Media.Imaging;
    using WpfApplication1.Key_Frame;

    public class MainModules
    {

        #region Attributes

        public VIDEO video; 

        public string VideoPath
        {
            get;
            set;
        }

        LowLevelModule lowLevelOutput;

        public volatile List<FaceRecognitionModule> faceModuleOutput;
        

        public List<LowLevelModule> LLMS;

        public List<List<string>> NamesPerKeyFrame, CatsPerKeyFrame;

        public double[] CutsPostition;

        public List<Audio_Recognition_Module> AudioCategoryList;

        public List<TimeSpan> ShotsInterval;

        public double[] CutPosSec;

        public string LIFPath
        {
            get;
            set;
        }

        public string KeyFramesFolderPath
        {
            get;
            set;
        }

        public string TrainDBPath
        {
            get;
            set;
        }

        public string LabelsPath
        {
            get;
            set;
        }

        public string RandomRanksPath
        {
            get;
            set;
        }

        public string AlignMatPath
        {
            get;
            set;
        }

        public int ClassNum
        {
            get;
            set;
        }

        public int PerSubjectNum
        {
            get;
            set;
        }

        CBVR.MainModules mainmodules;

        #endregion

        #region Constructors

        public MainModules()
        {
            this.mainmodules = new CBVR.MainModules();
        }

        public MainModules(CBVR.MainModules mainModuleObj)
        {
            this.mainmodules = mainModuleObj;
        }

        public MainModules(CBVR.MainModules mainModuleObj, string vidpath, string lifpath, string keyframespath,
                           string traindbpath, string labelspath, string rrankspath, string alignmatpath, int clsnum,
                            int persubjectnum) //, List<Audio_Recognition_Module> audiocategorylist)
        {
            this.mainmodules = mainModuleObj;
            this.VideoPath = vidpath;
            this.LIFPath = lifpath;
            this.KeyFramesFolderPath = keyframespath;
            this.TrainDBPath = traindbpath;
            this.LabelsPath = labelspath;
            this.RandomRanksPath = rrankspath;
            this.AlignMatPath = alignmatpath;
            this.ClassNum = clsnum;
            this.PerSubjectNum = persubjectnum;
            //this.AudioCategoryList = audiocategorylist;
        }

        #endregion

        public void RunMainModules()
        {
            #region Vari
            Stopwatch sw = new Stopwatch();
            MWArray[] outputs = new MWArray[9];
            double[,] clsnm = new double[1, 1], persubnm = new double[1, 1];
            clsnm[0, 0] = (double)this.ClassNum;
            persubnm[0, 0] = (double)this.PerSubjectNum;
            sw.Start();
            outputs = this.mainmodules.mainmodules(9, (MWArray)this.VideoPath, (MWArray)this.LIFPath, (MWArray)this.KeyFramesFolderPath,
                (MWArray)this.TrainDBPath, (MWArray)this.LabelsPath, (MWArray)this.RandomRanksPath,
                (MWArray)this.AlignMatPath, (MWNumericArray)this.ClassNum, (MWNumericArray)(double)this.PerSubjectNum);
            sw.Stop();
            var t = sw.Elapsed;



            MWCellArray LLF_Categories = (MWCellArray)outputs[0];
            MWNumericArray LLF_Probabilities = (MWNumericArray)outputs[1];
            MWNumericArray CategoryBounderiesSeconds = (MWNumericArray)outputs[2];
            MWNumericArray CutsPositions = (MWNumericArray)outputs[3];
            MWNumericArray MyCounter = (MWNumericArray)outputs[4];
            MWCellArray allnames = (MWCellArray)outputs[5];
            MWCellArray allcats = (MWCellArray)outputs[6];
            MWCellArray namesbound = (MWCellArray)outputs[7];
            MWCellArray catsbound = (MWCellArray)outputs[8];
            //Outputs Preparation
            #endregion

            #region Low Level

            List<string> LLFCategories_List = new List<string>();
            char[,] arr;
            for (int i = 0; i < LLF_Categories.NumberOfElements; i++)
            {
                arr = (char[,])LLF_Categories[i + 1].ToArray();
                LLFCategories_List.Add(new string(this.To1DArray(arr, 0)));
            }
            this.CutsPostition = this.To1DArray((double[,])CutsPositions.ToArray(), 0);
            List<BitmapImage> KeyFrames =
                this.ReadImagesFromDir(this.KeyFramesFolderPath, this.CutsPostition);

            double[,] Probability = (double[,])LLF_Probabilities.ToArray();
            double[,] CatBoundriesSeconds = (double[,])CategoryBounderiesSeconds.ToArray();
            CutPosSec=this.To1DArray(CatBoundriesSeconds, 0);
            
            double[,] Counter = (double[,])MyCounter.ToArray();

            //LLMS = 
            //this.lowLevelOutput = new LowLevelModule(KeyFrames, Probability, CatBoundriesSeconds, Counter);
            #endregion

            #region Face Recog
            int AllFramesCount = allnames.NumberOfElements, elementsPerFrameCount = 0;
            char[,] chararr;
            NamesPerKeyFrame = new List<List<string>>();
            CatsPerKeyFrame = new List<List<string>>();
            MWCellArray namessubcell, catssubcell;
            for (int i = 0; i < AllFramesCount; i++)
            {
                namessubcell = (MWCellArray)allnames[i + 1];
                catssubcell = (MWCellArray)allcats[i + 1];
                elementsPerFrameCount = namessubcell.NumberOfElements;
                NamesPerKeyFrame.Add(new List<string>());
                CatsPerKeyFrame.Add(new List<string>());
                if (namessubcell.IsEmpty)
                    continue;
                for (int j = 0; j < elementsPerFrameCount; j++)
                {
                    chararr = (char[,])allnames[i + 1][j + 1].ToArray();
                    NamesPerKeyFrame[i].Add(new string(this.To1DArray(chararr, 1)));

                    chararr = (char[,])allcats[i + 1][j + 1].ToArray();
                    CatsPerKeyFrame[i].Add(new string(this.To1DArray(chararr, 1)));
                }
            }

            LLM_FR_Splitter(KeyFrames, To1DArray(Probability, 0), To1DArray(Counter, 0), LLFCategories_List, NamesPerKeyFrame, CatsPerKeyFrame);

            /*List<string> NamesBound = new List<string>(), CatsBound = new List<string>();
            this.faceModuleOutput = new List<FaceRecognitionModule>();
            int NumOfBound = namesbound.NumberOfElements;
            for (int i = 0; i < NumOfBound; i++)
            {
                MWCellArray currentNamesBound = (MWCellArray)namesbound[i + 1];
                MWCellArray currentCatsBound = (MWCellArray)namesbound[i + 1];
                NamesBound = new List<string>();
                CatsBound = new List<string>();
                for (int j = 0; j < currentNamesBound.NumberOfElements; j++)
                {
                    MWCellArray currentNamesCell = (MWCellArray)currentNamesBound[j + 1];
                    MWCellArray currentCatsCell = (MWCellArray)currentCatsBound[j + 1];

                    //NamesBound[i].Add(new List<string>());
                    //CatsBound[i].Add(new List<string>());
                    for (int k = 0; k < currentNamesCell.NumberOfElements; k++)
                    {
                        chararr = (char[,])currentNamesCell[k + 1].ToArray();
                        NamesBound.Add(new string(this.To1DArray(chararr, 1)));

                        chararr = (char[,])currentCatsCell[k + 1].ToArray();
                        CatsBound.Add(new string(this.To1DArray(chararr, 1)));
                    }
                }
                this.faceModuleOutput.Add(new FaceRecognitionModule(NamesBound, CatsBound));
            }*/
            #endregion

            
        }

        public void startMakingDecision(List<Audio_Recognition_Module> AudioCategoryList)
        {
            video = new VIDEO(this.faceModuleOutput, this.LLMS, AudioCategoryList);                       
        }

        public void LLM_FR_Splitter(List<BitmapImage> KeyFrames, double[] Probability, 
                                                 double[] Counter, List<string> Categories,
                                                 List<List<string>> NamesPerKeyFrame, List<List<string>> CatsPerKeyFrame)
        {
            this.LLMS = new List<LowLevelModule>();
            List<KeyFrame> tempkeyframes;
            this.faceModuleOutput = new List<FaceRecognitionModule>();

            int N = 0;
            for (int i = 0; i < Counter.Length + 1; i++)
            {
                tempkeyframes = new List<KeyFrame>();
                LowLevelModule LLM = new LowLevelModule(Probability[i], Categories[i]);
                double NumbShots;
                                
                if (i == 0)
                    NumbShots = Counter[i];
                else if (i == (Counter.Length))
                    NumbShots = (KeyFrames.Count - 1) - Counter[i-1];     //Aya : Check 
                else
                    NumbShots = Counter[i] - Counter[i-1];
                
                
                for (int j = 0; j < NumbShots; j++)
                {
                    tempkeyframes.Add(new KeyFrame(KeyFrames[N], NamesPerKeyFrame[N], CatsPerKeyFrame[N]));
                    N++;
                }

                this.faceModuleOutput.Add(new FaceRecognitionModule(tempkeyframes));
                this.LLMS.Add(LLM);
            }
        }

        public List<BitmapImage> ReadImagesFromDir(string path, double[] CutsPositions)
        {
            List<BitmapImage> images = new List<BitmapImage>();

            for (int i = 0; i < CutsPositions.Length; i++)
            {
                images.Add(new BitmapImage(new Uri(path + @"\" + ((int)CutsPositions[i]).ToString() + ".png")));
            }
            return images;
        }

        private char[] To1DArray(char[,] array, int lengthIndex)
        {
            char[] ret = new char[array.GetLength(lengthIndex)];
            for (int i = 0; i < ret.Length; i++)
            {
                if (lengthIndex == 1)
                    ret[i] = array[0, i];
                else
                    ret[i] = array[i, 0];
            }
            return ret;
        }

        private double[] To1DArray(double[,] array, int lengthIndex)
        {
            double[] ret = new double[array.GetLength(lengthIndex)];
            for (int i = 0; i < ret.Length; i++)
            {
                if (lengthIndex == 1)
                    ret[i] = array[0, i];
                else
                    ret[i] = array[i, 0];
            }
            return ret;
        }
        
    }
}
