﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GP1.Adapter;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Controls;
using SpeechToText;

namespace GP1
{
    public class VIDEO
    {
        public List<Category> Cotegories = new List<Category>();

        public VIDEO(List<FaceRecognitionModule> faceModuleOutput, List<LowLevelModule> LLMS, List<Audio_Recognition_Module> AudioCategoryList)
        {
            for(int i = 0 ; i < LLMS.Count;i++)
            {
                Cotegories.Add(new Category(LLMS[i], faceModuleOutput[i], AudioCategoryList[i]));
            }             
        }
    }
}
